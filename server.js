//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var bodyParser = require('body-parser');
var requestJson = require('request-json');
var path = require('path');

app.use(bodyParser.json());
app.use(function(req,res, next){
  res.header("Access-control-Allow-Origin", "*");
  res.header("Access-control-Allow-Headers", "Origin, X-Requested-Width, Content-Type, Accept");
  next();
});
app.listen(port);

var urlMovimientos = "https://api.mlab.com/api/1/databases/bdbanca3mb79543/collections/movimientos?apiKey=mZ2kxESzW9M8AE53YNr87VT3lIk6ul9s";
var clienteMLab= requestJson.createClient(urlMovimientos);

console.log('todo list RESTful API server started on: ' + port);

app.get('/', function(req, res) {
  res.send("su peticion ha sido recibida cambiada");
});

app.get('/clientes', function(req, res) {
  var cliente1 = {id:"1", nombre:"Manuel", ciudad:"Mexico"};
  var cliente2 = {id:"2", nombre:"Giovanni", ciudad:"Oaxaca"};
  var cliente3 = {id:"3", nombre:"Victor", ciudad:"Hidalgo"};
  var clientes = [cliente1, cliente2, cliente3];
  res.send(clientes);
});

app.get('/clientes/:idCliente', function(req, res){
  res.send('Aquí tiene el cliente numero:' + req.params.idCliente);
});

app.post('/', function(req, res){
  res.send('su POST peticion ha sido recibida');
});

app.put('/', function(req, res){
  res.send('su PUT peticion ha sido recibida');
});

app.get('/movimientos', function(req,res){
 clienteMLab.get('',function(err,resM,body){
   if(err){
     console.log(err);
   }else{
     res.send(body);
   }
 });
});

app.post('/movimientos', function(req, res){
   clienteMLab.post('', req.body, function(err,resM,body){
     if(err){
       console.log(err);
     }else{
       res.send(body);
     }
   });
});

app.delete('/', function(req, res){
  res.send('su DELETE peticion ha sido recibida');
});
